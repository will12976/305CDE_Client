var myApp = angular.module('myApp', ['ngRoute'])
//Here I create my routes module.
myApp.config( ['$routeProvider', ($routeProvider) =>{
    $routeProvider
        //$routeProvider allows me to navigate between each page. Each route is referenced to the page where that /---- wants it to go.
        //Each page will contain a controller, where I can further use AngularJS, to create additional functions to each page. 
        .when('/filmsearch', {
            templateUrl: 'templates/film_search.html',
            controller: 'filmsearchController'
        })
        .when('/mapsearch', {
            templateUrl: 'templates/map_search.html',
            controller: 'mapsearchController'
        })
        //I have added additional parameters as a part of $routeParams. 
        //This passes the details from the third-party search
        .when('/details/:title/:year/:plot/:link/:genre/:id', {
            templateUrl: 'templates/details.html',
            controller: 'detailController'
        })
        //I have added additional parameters as a part of $routeParams. 
        //This passes the details from the third-party search
        .when('/detailsMap/:id/:add/:loc/:lat/:lng', {
            templateUrl: 'templates/detailsMap.html',
            controller: 'detailsMapController'
        })
        .when('/favourites', {
            templateUrl: 'templates/favourites.html',
            controller: 'favouritesController'
        })
        
        .when('/accounts', {
            templateUrl: 'templates/accounts.html',
            controller: 'accountController'
        })
        
        .when('/register', {
            templateUrl: 'templates/register.html',
            controller: 'registerController'
        })
        .when('/home', {
            templateUrl: 'templates/home.html',
            controller: 'homeController'
        })
        //This is where the page goes, when the page is first opened.
        .otherwise({
            redirectTo: 'home'
        })

    
}])
/* .run allows me to use $scope object of the same name in different controllers. 
Because each controller has its own functionality, it then has its own $scope objects. 
Using $rootScope, I can pass $scope objects from multiple controllers.

*/
.run(function($rootScope) {
    //Here I have a $rootScope object named test. This has a string
    $rootScope.test = 'Not Logged In'
    $rootScope.logout = ''
})

//$http meaning, it sends a http request to my api, to get the third-party data
//I require $rootScope within the controller parameters, as it needs to be referred outside of the controller.
myApp.controller('filmsearchController', ($scope, $http, $rootScope) =>{
    //Here I can call the $rootScope object here, by assigning a $scope object to $rootScope. It refers to this .run service.
    //This was important to have my authentication mechanism to make the user think that they are logged into the client.
    $scope.logout = $rootScope.logout
    $scope.test = $rootScope.test
    $scope.greeting = 'Welcome to the film search!'
    //What the user types in the search box, it takes an $event scope.
    $scope.filmSearch = ($event) => {
        //That tells it if the enter key is pressed
        if ($event.which == 13) {
            //It will run this code here
            var filmsearch = $scope.filmTerm
            var url = 'https://project1-will12976-2.c9users.io/films?title='+filmsearch
            //This adds the query at the end of my API's URL so that it searches for the film, via my API to the third-party API
            $http.get(url).success((response) =>{
                //If it returns a success, it will return the data
                $scope.films = response.data
                $scope.filmTerm = ''
            })
            
        }
        $scope.searched = 'You have searched for '
        $scope.sfilm = filmsearch
        
    }
})
//Same as my filmSearch. I take the $http service for my $http requests to my API, and a $rootScope that refers back to the $scope objects from different controllers.
myApp.controller('mapsearchController', ($scope, $http, $rootScope) =>{
    $scope.logout = $rootScope.logout
    $scope.test = $rootScope.test
    $scope.greeting = 'Welcome to the map search!'
    //I use the same process as before as the filmSearch
    $scope.mapSearch = ($event) => {
        if($event.which == 13) {
            var mapsearch = $scope.mapTerm
            //However I change the URL code to search for a country or place
            var url = 'https://project1-will12976-2.c9users.io/maps?address='+mapsearch
            $http.get(url).success((response) =>{
                //If it return the data, it is stored within a $scope object to be put inside of a html page
                $scope.maps = response.data
                $scope.mapTerm = ''
            })
        }
        $scope.searched = 'You have searched for '
        $scope.smap = mapsearch
    }
})
//Uses the $routeParams and $rootScope service
myApp.controller('detailController', ($scope, $routeParams, $rootScope) => {
    $scope.logout = $rootScope.logout
    $scope.test = $rootScope.test
    $scope.title = $routeParams.title
    $scope.plot = $routeParams.plot
    $scope.link = $routeParams.link
    $scope.genre = $routeParams.genre
    $scope.id = $routeParams.id
    $scope.year = $routeParams.year
    //$routeParams, takes the data that was searched from the /GET request.
    //What it basically does is, it prevents having to send another /GET request from another page, as the client won't know what the user has searched for
    //because it happened on a different page.
    
    //Here I have a $scope function of addToFavourites, that passes the title parameter from the film results.
    $scope.addToFavourites = (title) => {
        //Then is stored into localStorage, for access in my favourites page.
        localStorage.setItem(title,title)
        $scope.message = 'This has been added to your favourites'
    }
})
//Uses the $routeParams and $rootScope service
myApp.controller('detailsMapController', ($scope, $routeParams, $rootScope) =>{
    $scope.logout = $rootScope.logout
    $scope.test = $rootScope.test
    $scope.id = $routeParams.id
    $scope.address = $routeParams.add
    $scope.loc = $routeParams.loc
    $scope.lat = $routeParams.lat
    $scope.lng = $routeParams.lng
    //Same as my films details controller. I pass the data that was searched from the /GET request onto another page
    $scope.addToFavourites = (title) => {
        //Same as before I pass the title data, if the user decides to add it to favourites
        localStorage.setItem(title, title)
        //It then gets stored into localStorage
        $scope.message = 'This has been added to your favourites'
    }
})
//Still uses $rootScope for the accounts text.
myApp.controller('favouritesController', ($scope, $rootScope) =>{
    $scope.logout = $rootScope.logout
    $scope.test = $rootScope.test
    //This init function runs automatically, it pushes the data from localStorage into an array. 
    //We want an Array, as we can split it again using the ng-repeat directive.
    var init = function() {
        var items = Array()
        for (var a in localStorage) {
            items.push(localStorage[a])
        }
        $scope.films = items
    }
    init()
    //I have a delete function, so that if I delete a specific element, it will be removed from the page
    $scope.delete = (film) => {
        localStorage.removeItem(film)
        //When I clear an item, the page doesn't update its changes, so I need to refresh the page
        window.location.href = '#accounts'
        window.location.href = '#favourites'
        //I could have used window.location.reload(), however this breaks the authentication mechanism, because refreshed 'logs out' the account.
        //I used window.location.href to quickly switch pages to and from the favourites page, basically refreshing the page.
        //You don't see the effect as it takes a split-second to perform it
    }
    //I have a clearList, when clicked will clear the whole of localStorage.
    $scope.clearList = () =>{
        localStorage.clear()
        window.location.href = '#accounts'
        window.location.href = '#favourites'
        //I have used the same methods as before, as the page needs to reload, to see the changes.
    }

})
//Uses $rootScope
myApp.controller('homeController', ($scope, $rootScope) =>{
    $scope.logout = $rootScope.logout
    $scope.test = $rootScope.test
    //A nice little message, welcoming the user
    $scope.message = 'Welcome to my AngularJS Client!'
    
})

//Still uses $rootScope
myApp.controller('accountController', ($scope, $rootScope) => {
    
    //This is a small clear function, so that when the user hits the login button, the user and pass model i.e the textbox will reset to an empty textbox.
    $scope.clear = () => {
        //I have set them to an empty string i.e an empty textbox.
        $scope.user = ''
        $scope.pass = ''
    }
    //When the user hits the login button it runs this function.
    $scope.encrypt = () => {
       //It stores the user and pass ng-model in its own variable.
        var username = $scope.user
        var password = $scope.pass
        //It then encodes it using a Base64 encryption mechanism, called btoa.
        var encoded = btoa(username + password)
        
        //This section is a debug method. I found out that registering an account or logging in an account, when no accounts exist breaks the client.
        //Where $scope objects wouldn't work, nor adding stuff to localStorage.
        //So here I have a dummy username and password. I call this my admin account. 
        var adminUser = 'admin'
        var adminPass = 'pass'
        //It encrypts it.
        var encodeAdmin = btoa(adminUser + adminPass)
        //But this will only be stored if the length of localStorage is less than 1. In some cases, when the browser opens for the first time, this will 
        //be likely to run
        if(localStorage.length < 1){
            localStorage.setItem(encodeAdmin, encodeAdmin)
        }
        
        //Anyway, if the user and pass ng-model when becoming encoded, it will check all items in the localStorage, if there is a match in the system
        for (var i in localStorage) {
            if(i == encoded){
                //If it does, it welcomes the username
                window.alert('Welcome back ' + username + ' !')
                $scope.message = 'Successful Login'
                //Then it updates this $rootScope I talk about. This updates the text to logged in as: <username>
                //This will be shown in all pages, until it refreshes, i.e logs out.
                $rootScope.test = 'Logged in as: ' + username
                $rootScope.logout = 'Logout'
                break;
            } else {
                //If it doesn't find a match, it tells the user in the html file that it is invalid
                $scope.message = 'Invalid Username or Password'

            }
        }
    }
})
//Still uses the $rootScope
myApp.controller('registerController', ($scope, $rootScope) => {
    $scope.logout = $rootScope.logout
    $scope.test = $rootScope.test
    //When the user hits Sign Up, it will run this function
    $scope.register = () => {
        //Same again, it stores the ng-model of user and pass in its own variable.
        var username = $scope.user
        var password = $scope.pass
        
        //Debugging method - the same as the accountControler
        var adminUser = 'admin'
        var adminPass = 'pass'
        var encodeAdmin = btoa(adminUser + adminPass)
        
        //Here I have a form valiation that checks whether both the username and password contains enough characters to be stored in localStorage.
        //I have it so that the username and password must be equal or more than 5 character
        if(username.length <= 4 && password.length <= 4 ) {
            //If not, errors occur!
            window.alert('Account has not been created')
            $scope.error = 'Error! - '
            $scope.message = ' Both fields should be filled out, containing more than 5 characters'
        } else if(username.length >= 14 && password.length >= 14) {
            //If the username and password is more than 15 characters, errors happen again!
            window.alert('Account has not been created')
            $scope.error = 'Error! - '
            $scope.message = ' Both fields should have a character length of less than 15 characters'
        } else {
            //This is the debugging process that I have explained.
            var encoded = window.btoa(username + password)
            if(localStorage.length < 1) {
                localStorage.setItem(encodeAdmin, encodeAdmin)
            }
            //Now this sections checks for account duplications. It checks each item in localStorage. If it finds a match...
            for (var i in localStorage){
                if(i == encoded){
                    //It returns an error! :(
                    window.alert('This account already exists, please enter a new account')
                    $scope.error = ''
                    $scope.message = 'This account already exists, please enter a new account'
                    break
                } else {
                    //If not, it says account created! and it sets it in localStorage
                    $scope.error = ''
                    localStorage.setItem(encoded, encoded)
                    $scope.message = "Account Created"
                }
            }
        }
    }
})
