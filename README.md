# Documentation:

Here I will discuss what each thing does in my code, when I use my client to display third-party data from my API.


Take a look here:
```
<head>
    <title>Home Page</title>
    <script src="js/angular.js"></script>
    <script src="js/angular-route.js"></script>
    <script src="js/routes.js"></script>
    <link rel="stylesheet" href="css/styles.css">
</head>

```
This is where I keep my library source references and other files such as css. Since I am making single-page 
applications, I will only use this referencing on my home page. My files, angular.js and angular-route.js are 
my library files, this is essentially the programming language to build my client, so vital to include.

Next I move onto the body of my HTML page. Since I want all my pages to link up, I will use the ng-app directive.
This is specifically designed to talk to my routes.js file, to reference the various pages, and how I can cross
between pages. I have intialized my routing and called it **'myApp'.**

```
<body ng-app="myApp">
```
```
var myApp = angular.module('myApp', ['ngRoute'])
```
The ngRoute establishes the routing services for my directives. So now my index.html being the base webpage is
now linked into my routing page, I can now start to link up my webpage.

```
myApp.config( ['$routeProvider', ($routeProvider) =>{
    $routeProvider

        .when('/filmsearch', {
            templateUrl: 'templates/film_search.html',
            controller: 'filmsearchController'
        })
}])
```
To take reference to a different page, we first need a configuration setup, so for when I click on a button, it
can check the routes file and opens up the desired page. For this instance, I have another page called 
/filmsearch. I must specify the location url to the file, and a controller. Controller are used for the use of
AngularJS in your webpage, as it injects itself into the DOM, using the directive **ng-controller**.
Going back to how I can link up webpages, I use the '#' symbol to reference instead of '/' like below.
```
Navigation | <a href="#filmsearch">Film Search</a></p>
```

Now I have multiple pages in one single-page applications, I now want to use my new page as my film search.
I would first create a textbox, for my search, which takes a directive called **ng-model**. With this, I can
take whatever film that has been inputted, and it will store itself in its own scope. I would want it to do
this when the user hits enter.
I will need this to create this query that I need for when I search for film data via my API.
Looking below, my **ng-model** is equalled to **filmTerm**. This automatically creates a scope object,
**$scope.filmTerm**, and I will know that it will be equalled to whatever has been inputted.

```
<p><input type="text" ng-model="filmTerm" ng-keydown="filmSearch($event)" autofocus></p>

```
The directive **ng-keydown** will be this execution for when the user hits enter, and if they do, it
will run the function called filmSearch, which takes a **$event** parameter. With $event, we can access
our event, and tell it what actions it needs to perform. In this case we want it to run a function when 
the 'Enter' key is pressed. We can add this into the controller like so:

```
if ($event.which == 13) {  //Code 13 equates to the enter button
```

Now, this is where it gets the important part, getting it to connect to my API, to receieve third-party data.
Within the $event of when the Enter key is pressed, we want it to send a /GET request to my api, following my
API's url, passed with the query that has been entered into the textbox.

```
        var filmsearch = $scope.filmTerm
        var url = 'https://project1-will12976-2.c9users.io/films?title='+filmsearch
        $http.get(url).success((response) =>{
            $scope.films = response.data
            $scope.filmTerm = ''
        })
```
Notice how everything pieces together. We notice the ng-model from our search.html has it own scope, but is now
assigned to a variable called **filmsearch**. We can attach this to my base URL for my API, in a variable called
**url**. Again, notice that my variable filmsearch is attached to the end of my query structure. This completes
the whole URL, to collect this third-party data. But now we need it to send this /GET request.
Luckily, I can use a service called $http, which sends a HTTP request taking the parameter of my completed url.
and if it suceeds it will be equal to the parameter **response**. I now get the data back, so I want this stored
in a $scope, so that I can refer to it in my .html page, called $scope.films

```
$scope.films = response.data
```
Now we want this to display onto the webpage. I use a new direct **ng-repeat**, which acts like a **for each in**
statement. So we take $scope.films and splits each result seperately, extracting the Title, Year, Genre and Description.
Remember in my API, I mapped out the third-party request, so it only takes types of data I want, so I need to refer to 
each detail from my API.

```
<table style="width:100%">
    <tr>
        <th>Title</th>
        <th>Genre</th>
        <th>Description</th>
        <th>Thumbnail</th>
        <th>More Info</th>
    </tr>
    <tr ng-repeat="film in films">
        <p>{{ searched }}<b>{{ sfilm }}</b></p>
        <td>{{film.title}} ({{film.year}})</td><td>{{film.genre}}</td><td> {{film.description}}</td><td><img ng-src="{{film.poster}}"/></td>
    </tr>
</table>
```
So now I want a details page linked in with my search, to give further information to a certain film, and whether I want to store is as a favourite film.
I create a link, which links to the details page, but it needs to pass the data with it too, so again I can extract the third-party data and display it
on my page.

```
<td><a href="#details/{{film.title}}/{{film.year}}/{{film.plot}}/{{film.rating}}/{{film.genre}}/{{film.id}}">View</a></td>
```
Here I still uses the for film in films statement, to specify the film and the data associated with it. But now I added extra details about that film.
Remember to used **a href** to link and specify to another pages, and use the **#** symbol instead of the forward-slash. Each detail has to be assigned
in the roots folder.

```
myApp.controller('detailController', ($scope, $routeParams) => {
    
    $scope.title = $routeParams.title
    $scope.plot = $routeParams.plot
    $scope.link = $routeParams.link
    $scope.genre = $routeParams.genre
    $scope.id = $routeParams.id
    $scope.year = $routeParams.year
    
})
```
Above, is how I specify each detail to the film. It uses $routeParams to allow this $scope to pass through to the page it is assigned to.
This is how it would link up.

```
    .when('/details/:title/:year/:plot/:link/:genre/:id', {
        templateUrl: 'templates/details.html',
        controller: 'detailController'
})
```
To link the $scope to the route, you would use **:title** if you specified $routeParams.title etc, and you would call the $scope.title in
the webpage.

Now, this should link up to a details page, passing through the details of the film. So I have sucessfully communicated to each webpage using 
this $routeParams service.

Next, I move onto creating a favourites page for my client, so if I like a type of film or map (From my map search), I can store the details,
so I can trace back to it afterwards in case I forget the name. In this case, I want this to be added to my details page. So I start off by
creating a button, that takes a new directive **ng-click**, which designed to run Angular services when it is clicked.

```
<p><button  ng-click="addToFavourites(title)">Add to Favourites</button></p>
```

In this example, clicking on the button will run the $scope function **addtoFavourites()**, in which is linked to my detailController, which will
execute its function, that takes the parameter in which is taken from the search, in this case will be **title** and further used to add to a 
storage facility. In this example, I have used LocalStorage to store my title favourite data like so:

```
$scope.addToFavourites = (title) => {
    localStorage.setItem(title, title)
    $scope.message = 'This has been added to your favourites'
}
```
To store my film / map title I use **localStorage.setItem()**, taking two parameters holding the key-name and key-value. Each time I add to my
favourites, it will create this new key with the titlename. So that completes my favourites page.

Next I need to find a way of deleting a favourite, if I don't want it anymore, so I will use the same structure as before, where I added to my
favourite to **localStorage**. I create a button as before using **ng-click**, which this time takes a parameter to a $scope function called **clearList**.
Luckily, there was a handy method at the time called **localStorage.clear()**, where it clears the whole list, however when I cleared it, it wouldn't
refresh the page. To avoid this issue, I used window.location.reload(); where it refreshes the page, seeing the change.

Finally, to remove a certain item from the list, I need to use the **ng-repeat** directive again, 